{ pkgs ? import <nixpkgs> { } }:

let common = import ./common-dependencies.nix { inherit pkgs; };
in with pkgs;

mkShell { buildInputs = common ++ [ just nixfmt ]; }
