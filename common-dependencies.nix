{ pkgs ? import <nixpkgs> { }, ... }:

with pkgs; [
  rustup
  wasm-pack
]
