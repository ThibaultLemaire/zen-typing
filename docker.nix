{ pkgs ? import <nixpkgs> { } }:

let common = import ./common-dependencies.nix { inherit pkgs; };
in with pkgs;

dockerTools.buildImage {
  name = "zen-typing-nix";
  tag = "latest";

  contents = common ++ [
    busybox
    gcc
    cacert # So rustup can https
  ];

  config.Cmd = [ "/bin/sh" ];
}
