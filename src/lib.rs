use rand::{distributions::weighted::alias_method::WeightedIndex, prelude::*};
use std::{
    cell::RefCell,
    collections::{HashMap, VecDeque},
    convert::{TryFrom, TryInto},
    iter::FromIterator,
    ops::Deref,
};
use wasm_bindgen::{convert::FromWasmAbi, prelude::*, JsCast};
use web_sys::{
    CssStyleDeclaration, Document, Element, HtmlElement, KeyboardEvent, KeyboardEventInit, Window,
};
#[macro_use]
extern crate lazy_static;

const ROWS: u8 = 6;

const ENGLISH_FREQ: [f32; 32] = [
    // From Lee, E. Stewart. "Essays about Computer Security" (PDF).
    // University of Cambridge Computer Laboratory. p. 181.
    // http://www.cl.cam.ac.uk/~mgk25/lee-essays.pdf
    12.17, // ' ' 0
    1.64,  // "'" 1 (6.57 / 4, Bad guess based on the "others" frequency)
    1.64,  // ',' 2 (6.57 / 4)
    1.64,  // '.' 3 (6.57 / 4)
    0.82,  // '/' 4 (6.57 / 8)
    0.82,  // ';' 5 (6.57 / 8)
    6.09,  // 'a' 6
    1.05,  // 'b' 7
    2.84,  // 'c' 8
    2.92,  // 'd' 9
    11.36, // 'e' 10
    1.79,  // 'f' 11
    1.38,  // 'g' 12
    3.41,  // 'h' 13
    5.44,  // 'i' 14
    0.24,  // 'j' 15
    0.41,  // 'k' 16
    2.92,  // 'l' 17
    2.76,  // 'm' 18
    5.44,  // 'n' 19
    6.00,  // 'o' 20
    1.95,  // 'p' 21
    0.24,  // 'q' 22
    4.95,  // 'r' 23
    5.68,  // 's' 24
    8.03,  // 't' 25
    2.43,  // 'u' 26
    0.97,  // 'v' 27
    1.38,  // 'w' 28
    0.24,  // 'x' 29
    1.30,  // 'y' 30
    0.03,  // 'z' 31
];

lazy_static! {
    static ref ENG_DVORAK: WeightedIndex<f32> = WeightedIndex::new(vec![
        ENGLISH_FREQ[5],  // lld-
        ENGLISH_FREQ[6],  // llr-
        ENGLISH_FREQ[1],  // llu-
        ENGLISH_FREQ[22], // rld-
        ENGLISH_FREQ[20], // rlr-
        ENGLISH_FREQ[2],  // rlu-
        ENGLISH_FREQ[15], // mld-
        ENGLISH_FREQ[10], // mlr-
        ENGLISH_FREQ[3],  // mlu-
        ENGLISH_FREQ[16], // ildc
        ENGLISH_FREQ[26], // ilrc
        ENGLISH_FREQ[21], // iluc
        ENGLISH_FREQ[29], // ilds
        ENGLISH_FREQ[14], // ilrs
        ENGLISH_FREQ[30], // ilus
        ENGLISH_FREQ[0],  // t---
        ENGLISH_FREQ[11], // irus
        ENGLISH_FREQ[9],  // irrs
        ENGLISH_FREQ[7],  // irds
        ENGLISH_FREQ[12], // iruc
        ENGLISH_FREQ[13], // irrc
        ENGLISH_FREQ[18], // irdc
        ENGLISH_FREQ[8],  // mru-
        ENGLISH_FREQ[25], // mrr-
        ENGLISH_FREQ[28], // mrd-
        ENGLISH_FREQ[23], // rru-
        ENGLISH_FREQ[19], // rrr-
        ENGLISH_FREQ[27], // rrd-
        ENGLISH_FREQ[17], // lru-
        ENGLISH_FREQ[24], // lrr-
        ENGLISH_FREQ[31], // lrd-
    ]).unwrap();

    static ref FLAT_DIST: WeightedIndex<f32> = WeightedIndex::new(vec![
        1., // lld-
        1., // llr-
        1., // llu-
        1., // rld-
        1., // rlr-
        1., // rlu-
        1., // mld-
        1., // mlr-
        1., // mlu-
        1., // ildc
        1., // ilrc
        1., // iluc
        1., // ilds
        1., // ilrs
        1., // ilus
        1., // t---
        1., // irus
        1., // irrs
        1., // irds
        1., // iruc
        1., // irrc
        1., // irdc
        1., // mru-
        1., // mrr-
        1., // mrd-
        1., // rru-
        1., // rrr-
        1., // rrd-
        1., // lru-
        1., // lrr-
        1., // lrd-
    ]).unwrap();

    static ref ENG_QWERTY: WeightedIndex<f32> = WeightedIndex::new(vec![
        ENGLISH_FREQ[31], // lld-
        ENGLISH_FREQ[6],  // llr-
        ENGLISH_FREQ[22], // llu-
        ENGLISH_FREQ[29], // rld-
        ENGLISH_FREQ[24], // rlr-
        ENGLISH_FREQ[28], // rlu-
        ENGLISH_FREQ[8],  // mld-
        ENGLISH_FREQ[9],  // mlr-
        ENGLISH_FREQ[10], // mlu-
        ENGLISH_FREQ[27], // ildc
        ENGLISH_FREQ[11], // ilrc
        ENGLISH_FREQ[23], // iluc
        ENGLISH_FREQ[7],  // ilds
        ENGLISH_FREQ[12], // ilrs
        ENGLISH_FREQ[25], // ilus
        ENGLISH_FREQ[0],  // t---
        ENGLISH_FREQ[30], // irus
        ENGLISH_FREQ[13], // irrs
        ENGLISH_FREQ[19], // irds
        ENGLISH_FREQ[26], // iruc
        ENGLISH_FREQ[15], // irrc
        ENGLISH_FREQ[18], // irdc
        ENGLISH_FREQ[14], // mru-
        ENGLISH_FREQ[16], // mrr-
        ENGLISH_FREQ[2],  // mrd-
        ENGLISH_FREQ[20], // rru-
        ENGLISH_FREQ[17], // rrr-
        ENGLISH_FREQ[3],  // rrd-
        ENGLISH_FREQ[21], // lru-
        ENGLISH_FREQ[5],  // lrr-
        ENGLISH_FREQ[4],  // lrd-
    ]).unwrap();
}

#[derive(PartialEq, Eq, Clone, Hash)]
enum FingerPosition {
    Up,
    Rest,
    Down,
}

#[derive(PartialEq, Eq, Clone, Hash)]
enum IndexPosition {
    Centre,
    Side,
}

#[derive(PartialEq, Eq, Clone, Hash)]
enum Hand {
    Left,
    Right,
}

#[derive(PartialEq, Eq, Clone, Hash)]
enum Key {
    Thumb,
    Index(Hand, FingerPosition, IndexPosition),
    Middle(Hand, FingerPosition),
    Ring(Hand, FingerPosition),
    Little(Hand, FingerPosition),
}

macro_rules! _key_from_str {
    ($( $key:expr => $label:pat => $code:pat), +) => {
        impl TryFrom<&str> for Key {
            type Error = &'static str;
            fn try_from(value: &str) -> Result<Self, Self::Error> {
                match value {
                    $($code => Ok($key),)+
                    $($label => Ok($key),)+
                    _ => Err("Unmapped key"),
                }
            }
        }
    }
}

macro_rules! _str_from_key {
    ($( $key:pat => $label:pat => $code:expr), +) => {
        impl From<&Key> for &'static str {
            fn from(value: &Key) -> Self {
                match value {
                    $($key => $code,)+
                }
            }
        }
    }
}

impl TryFrom<&KeyboardEvent> for Key {
    type Error = &'static str;

    fn try_from(kbevent: &KeyboardEvent) -> Result<Self, Self::Error> {
        if kbevent.repeat() {
            return Err("Repeating key ignored");
        }
        kbevent.code().as_str().try_into()
    }
}

macro_rules! _random_distribution {
    ($( $key:expr => $label:pat => $code:expr), +) => {
        const ALL_KEYS:  [Key; 31] = [ $($key,)+ ];
    }
}

macro_rules! map_keys {
    ($($anything:tt)+) => {
        _key_from_str!($($anything)+);
        _str_from_key!($($anything)+);
        _random_distribution!($($anything)+);
    };
}

use FingerPosition::*;
use Hand::*;
use IndexPosition::*;
use Key::*;

map_keys! {
    Little(Left, Down) => "lld-" => "KeyZ",
    Little(Left, Rest) => "llr-" => "KeyA",
    Little(Left, Up) => "llu-" => "KeyQ",
    Ring(Left, Down) => "rld-" => "KeyX",
    Ring(Left, Rest) => "rlr-" => "KeyS",
    Ring(Left, Up) => "rlu-" => "KeyW",
    Middle(Left, Down) => "mld-" => "KeyC",
    Middle(Left, Rest) => "mlr-" => "KeyD",
    Middle(Left, Up) => "mlu-" => "KeyE",
    Index(Left, Down, Centre)  => "ildc"=> "KeyV",
    Index(Left, Rest, Centre) => "ilrc" => "KeyF",
    Index(Left, Up, Centre) => "iluc" => "KeyR",
    Index(Left, Down, Side) => "ilds" => "KeyB",
    Index(Left, Rest, Side) => "ilrs" => "KeyG",
    Index(Left, Up, Side) => "ilus" => "KeyT",
    Thumb => "t---" => "Space",
    Index(Right, Up, Side) => "irus" => "KeyY",
    Index(Right, Rest, Side) => "irrs" => "KeyH",
    Index(Right, Down, Side) => "irds" => "KeyN",
    Index(Right, Up, Centre) => "iruc" => "KeyU",
    Index(Right, Rest, Centre) => "irrc" => "KeyJ",
    Index(Right, Down, Centre) => "irdc" => "KeyM",
    Middle(Right, Up)  => "mru-"=> "KeyI",
    Middle(Right, Rest) => "mrr-" => "KeyK",
    Middle(Right, Down) => "mrd-" => "Comma",
    Ring(Right, Up) => "rru-" => "KeyO",
    Ring(Right, Rest) => "rrr-" => "KeyL",
    Ring(Right, Down) => "rrd-" => "Period",
    Little(Right, Up) => "lru-" => "KeyP",
    Little(Right, Rest) => "lrr-" => "Semicolon",
    Little(Right, Down) => "lrd-" => "Slash"
}

struct Glyph {
    root_svg_element: HtmlElement,
    child_use_element: Element,
}

impl Glyph {
    fn set_class(&self, name: &str) {
        self.root_svg_element
            .set_attribute("class", name)
            .expect("couldn't set class")
    }

    fn set_column(&self, value: &Key) {
        let (finger, hand, position, side) = match value {
            Thumb => ("thumb", None, &Rest, false),
            Index(hand, position, side) => (
                "index",
                Some(hand),
                position,
                match side {
                    Centre => false,
                    Side => true,
                },
            ),
            Middle(hand, position) => ("middle", Some(hand), position, false),
            Ring(hand, position) => ("ring", Some(hand), position, false),
            Little(hand, position) => ("little", Some(hand), position, false),
        };

        let side = match (side, hand) {
            (true, Some(Left)) => "-right",
            (true, Some(Right)) => "-left",
            _ => "",
        };
        let hand = match hand {
            None => "",
            Some(Left) => "left-",
            Some(Right) => "right-",
        };

        self.root_svg_element
            .set_attribute("column", &format!("{}{}", hand, finger))
            .expect("couldn't set column");

        let position = match position {
            Rest => "",
            Up => "-up",
            Down => "-down",
        };
        self.child_use_element
            .set_attribute("href", &format!("#{}{}{}", finger, side, position))
            .expect("couldn't set href");
    }

    fn set_row(&self, value: u8) {
        self.root_svg_element
            .set_attribute("row", &format!("r{}", value))
            .expect("couldn't set row");
    }
}

struct OnBoard {
    glyph: Glyph,
    key: Key,
}

impl OnBoard {
    fn new(glyph: Glyph, row: u8, key: Key) -> Self {
        glyph.set_class("on-board");
        glyph.set_column(&key);
        glyph.set_row(row);
        Self { glyph, key }
    }

    fn set_row(&self, value: u8) {
        self.glyph.set_row(value);
    }

    fn hittable(self) -> Hittable {
        Hittable::new(self.glyph, self.key)
    }
}

struct Hittable {
    glyph: Glyph,
    key: Key,
}

impl Hittable {
    fn new(glyph: Glyph, key: Key) -> Self {
        glyph.set_row(0);

        Self { glyph, key }
    }

    fn key(&self) -> &Key {
        &self.key
    }

    fn free(self) -> Free {
        Free::new(self.glyph)
    }
}

struct Free(Glyph);

impl Free {
    fn new(glyph: Glyph) -> Self {
        glyph.set_class("free");
        Self(glyph)
    }

    fn ready(self, key: Key) -> Ready {
        Ready::new(self.0, key)
    }
}

struct Ready {
    glyph: Glyph,
    key: Key,
}

impl Ready {
    fn new(glyph: Glyph, key: Key) -> Self {
        glyph.set_class("ready");
        glyph.set_column(&key);
        glyph.set_row(ROWS - 1);
        Self { glyph, key }
    }

    fn inject(self) -> OnBoard {
        let glyph = self.glyph;
        glyph.set_class("on-board");
        OnBoard {
            glyph,
            key: self.key,
        }
    }
}

struct GenericVirtualKey {
    element: Element,
    keydown: Closure<dyn FnMut()>,
    keyup: Closure<dyn FnMut()>,
}

impl GenericVirtualKey {
    fn new(code: &str, element: Element) -> Self {
        let build_kb_event_dispatcher = |kb_event| {
            let dispatcher = element.clone();
            let kb_event = KeyboardEvent::new_with_keyboard_event_init_dict(
                kb_event,
                KeyboardEventInit::new().bubbles(true).code(code),
            )
            .expect("couldn't create keyboard event");
            Closure::wrap(Box::new(move || {
                dispatcher
                    .dispatch_event(kb_event.as_ref())
                    .expect("couldn't dispatch event");
            }) as Box<dyn FnMut()>)
        };
        let new = Self {
            keydown: build_kb_event_dispatcher("keydown"),
            keyup: build_kb_event_dispatcher("keyup"),
            element,
        };
        new.plug_cb();
        new
    }

    fn plug_cb(&self) {
        let add_listener = |event, callback| {
            self.element
                .add_event_listener_with_callback(event, callback)
                .expect(&format!("couldn't add {} listener to virtual key", event));
        };
        let keydown = self.keydown.as_ref().unchecked_ref();
        let keyup = self.keyup.as_ref().unchecked_ref();
        add_listener("touchstart", keydown);
        add_listener("mousedown", keydown);
        add_listener("touchend", keyup);
        add_listener("touchcancel", keyup);
        add_listener("mouseup", keyup);
    }

    fn hold(&self) {
        self.element
            .set_attribute("held", "down")
            .expect("couldn't set held=down");
    }
    fn release(&self) {
        self.element
            .set_attribute("held", "up")
            .expect("couldn't set held=up");
    }
}

struct VirtKey(GenericVirtualKey);

impl Deref for VirtKey {
    type Target = GenericVirtualKey;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl VirtKey {
    fn new(key: &Key, element: Element) -> Self {
        let virtual_key = GenericVirtualKey::new(key.into(), element);
        virtual_key
            .element
            .add_event_listener_with_callback(
                "mouseleave",
                virtual_key.keyup.as_ref().unchecked_ref(),
            )
            .unwrap();
        Self(virtual_key)
    }

    fn glow_on(&self) {
        self.element
            .set_attribute("glow", "orange")
            .expect("couldn't set custom glow attribute");
    }
    fn glow_off(&self) {
        self.element
            .remove_attribute("glow")
            .expect("couldn't remove custom glow attribute");
    }
}

struct MenuItem(GenericVirtualKey);

impl Deref for MenuItem {
    type Target = GenericVirtualKey;

    fn deref(&self) -> &Self::Target {
        &self.0
    }
}

impl MenuItem {
    fn new(key: &Key, element: Element) -> Self {
        Self(GenericVirtualKey::new(key.into(), element))
    }

    fn greyed(&self) {
        self.element
            .set_attribute("greyed", "out")
            .expect("couldn't set custom greyed attribute");
    }
    fn colored(&self) {
        self.element
            .remove_attribute("greyed")
            .expect("couldn't remove custom greyed attribute");
    }
}

trait DocumentExtensions {
    fn glyphs(&self) -> Vec<Glyph>;
    fn virtual_keys(&self) -> HashMap<Key, VirtKey>;
}
impl DocumentExtensions for Document {
    fn glyphs(&self) -> Vec<Glyph> {
        let glyphs = self.get_elements_by_class_name("ready");
        (0u32..glyphs.length())
            .map(|index| {
                let root_svg_element: HtmlElement = glyphs.item(index).unwrap().unchecked_into();
                let child_use_element =
                    root_svg_element.children().item(0).expect("no 'use' child");
                Glyph {
                    root_svg_element,
                    child_use_element,
                }
            })
            .collect()
    }

    fn virtual_keys(&self) -> HashMap<Key, VirtKey> {
        let virtual_keys = self
            .get_element_by_id("virtual-kb")
            .expect("virtual keyboard not found")
            .children();
        assert_eq!(virtual_keys.length(), 33);
        HashMap::from_iter((0u32..33).filter_map(|index| {
            let element = virtual_keys.item(index).unwrap();
            match element.id().as_str().try_into() {
                Ok(key) => {
                    let virt_key = VirtKey::new(&key, element);
                    Some((key, virt_key))
                }
                Err(_) => None,
            }
        }))
    }
}

fn make_closure<T: FromWasmAbi + 'static>(
    callback: impl FnMut(T) + 'static,
) -> Closure<dyn FnMut(T)> {
    Closure::wrap(Box::new(callback) as Box<dyn FnMut(T)>)
}

struct KeyGenerator {
    rng: ThreadRng,
    distribution: &'static WeightedIndex<f32>,
}

impl KeyGenerator {
    fn new() -> Self {
        Self {
            rng: thread_rng(),
            distribution: &ENG_DVORAK,
        }
    }

    fn generate_key(&mut self) -> Key {
        ALL_KEYS[self.distribution.sample(&mut self.rng)].clone()
    }
}

struct Game {
    rkg: KeyGenerator,
    show_virtual_keyboard: bool,
    virtual_keys: HashMap<Key, VirtKey>,
    free: Option<Free>,
    ready: Option<Ready>,
    queue: VecDeque<OnBoard>,
    front: Option<Hittable>,
    root_style: CssStyleDeclaration,
    vkb_toggle: MenuItem,
    btn_dist_flat: MenuItem,
    btn_dist_eng_dvorak: MenuItem,
    btn_dist_eng_qwerty: MenuItem,
}

impl Game {
    fn new(document: &Document) -> Self {
        let mut rkg = KeyGenerator::new();
        let free;
        let ready;
        let mut queue: VecDeque<OnBoard>;
        let front;
        {
            let mut glyphs = document.glyphs();
            free = Some(Free::new(glyphs.pop().unwrap()));
            ready = Some(Ready::new(glyphs.pop().unwrap(), rkg.generate_key()));
            queue = glyphs
                .drain(..)
                .enumerate()
                .map(|(i, glyph)| OnBoard::new(glyph, i as u8 + 1, rkg.generate_key()))
                .collect();
            front = Some(queue.pop_back().unwrap().hittable());
        }
        let virtual_keys = document.virtual_keys();
        virtual_keys
            .get(&front.as_ref().unwrap().key)
            .unwrap()
            .glow_on();
        let root_element: HtmlElement = document
            .document_element()
            .expect("no document element")
            .unchecked_into();
        let vkb_toggle = MenuItem::new(
            &Thumb,
            document
                .get_element_by_id("☑️⌨️")
                .expect("virtual keyboard toggle not found")
                .unchecked_into(),
        );
        let btn_dist_flat = MenuItem::new(
            &Index(Left, Rest, Centre),
            document
                .get_element_by_id("☑️🎲=")
                .expect("flat distribution button not found")
                .unchecked_into(),
        );
        let btn_dist_eng_dvorak = MenuItem::new(
            &Index(Left, Up, Centre),
            document
                .get_element_by_id("☑️🎲eng-dvorak")
                .expect("English Dvorak distribution button not found")
                .unchecked_into(),
        );
        let btn_dist_eng_qwerty = MenuItem::new(
            &Index(Left, Down, Centre),
            document
                .get_element_by_id("☑️🎲eng-qwerty")
                .expect("English QWERTY distribution button not found")
                .unchecked_into(),
        );
        Self {
            rkg,
            show_virtual_keyboard: true,
            free,
            ready,
            queue,
            front,
            virtual_keys,
            root_style: root_element.style(),
            vkb_toggle,
            btn_dist_flat,
            btn_dist_eng_dvorak,
            btn_dist_eng_qwerty,
        }
    }

    fn set_playground_height(&self, height: &str) {
        self.root_style
            .set_property("--playground-height", height)
            .expect("couldn't set style property");
    }

    fn toggle_vkb(&mut self) {
        self.show_virtual_keyboard = !self.show_virtual_keyboard;
        if self.show_virtual_keyboard {
            self.set_playground_height("70vh");
            self.vkb_toggle.colored();
        } else {
            self.set_playground_height("96vh");
            self.vkb_toggle.greyed();
        }
    }

    fn on_keydown_in_playground(&mut self, kbevent: KeyboardEvent) {
        if let Ok(key) = (&kbevent).try_into() {
            self.virtual_keys.get(&key).unwrap().hold();
            if key == *self.front.as_ref().unwrap().key() {
                // Disable browser shortcuts
                // https://gitlab.com/ThibaultLemaire/zen-typing/-/issues/2
                kbevent.prevent_default();

                let front_to_be = self.queue.pop_front().unwrap().hittable();
                let next_target_key = self.virtual_keys.get(&front_to_be.key).unwrap();
                let prev_front = self.front.replace(front_to_be).unwrap();
                self.virtual_keys.get(&prev_front.key).unwrap().glow_off();
                next_target_key.glow_on();
                let prev_free = self.free.replace(prev_front.free());
                let next_key: Key = self.rkg.generate_key();
                let prev_ready = self.ready.replace(prev_free.unwrap().ready(next_key));
                self.queue.push_back(prev_ready.unwrap().inject());
                for (i, glyph) in self.queue.iter().enumerate() {
                    glyph.set_row((i + 1) as _);
                }
            }
        }
    }

    fn on_keyup_in_playground(&self, kbevent: KeyboardEvent) {
        if let Ok(key) = (&kbevent).try_into() {
            self.virtual_keys.get(&key).unwrap().release();
        }
    }

    fn on_keydown_in_main_menu(&self, kbevent: KeyboardEvent) {
        match (&kbevent).try_into() {
            Ok(Thumb) => self.vkb_toggle.hold(),
            Ok(Index(_, Rest, Centre)) => self.btn_dist_flat.hold(),
            Ok(Index(_, Up, Centre)) => self.btn_dist_eng_dvorak.hold(),
            Ok(Index(_, Down, Centre)) => self.btn_dist_eng_qwerty.hold(),
            _ => {}
        }
    }

    fn on_keyup_in_main_menu(&mut self, kbevent: KeyboardEvent) {
        match (&kbevent).try_into() {
            Ok(Thumb) => {
                self.toggle_vkb();
                self.vkb_toggle.release();
            }
            Ok(Index(_, Rest, Centre)) => {
                self.rkg.distribution = &FLAT_DIST;
                self.btn_dist_flat.release();
                self.btn_dist_flat.colored();
                self.btn_dist_eng_dvorak.greyed();
                self.btn_dist_eng_qwerty.greyed();
            }
            Ok(Index(_, Up, Centre)) => {
                self.rkg.distribution = &ENG_DVORAK;
                self.btn_dist_eng_dvorak.release();
                self.btn_dist_eng_dvorak.colored();
                self.btn_dist_flat.greyed();
                self.btn_dist_eng_qwerty.greyed();
            }
            Ok(Index(_, Down, Centre)) => {
                self.rkg.distribution = &ENG_QWERTY;
                self.btn_dist_eng_qwerty.release();
                self.btn_dist_eng_qwerty.colored();
                self.btn_dist_flat.greyed();
                self.btn_dist_eng_dvorak.greyed();
            }
            _ => {}
        }
    }
}

enum Scene {
    Playground,
    MainMenu,
}

struct SceneManager {
    state: &'static RefCell<Game>,
    window: Window,
    document: Document,
    scene: Scene,
    playground: HtmlElement,
    main_menu: HtmlElement,
    escape: GenericVirtualKey,
    playground_keydown: Closure<dyn FnMut(KeyboardEvent)>,
    vkb_keyup: Closure<dyn FnMut(KeyboardEvent)>,
    main_menu_keydown: Closure<dyn FnMut(KeyboardEvent)>,
    main_menu_keyup: Closure<dyn FnMut(KeyboardEvent)>,
}

impl SceneManager {
    fn new(window: Window) -> Self {
        let document = window.document().expect("no document on window");
        let game: &_ = Box::leak(Box::new(RefCell::new(Game::new(&document))));

        let playground: HtmlElement = document
            .get_element_by_id("playground")
            .expect("playground not found")
            .unchecked_into();
        let main_menu: HtmlElement = document
            .get_element_by_id("main-menu")
            .expect("main menu not found")
            .unchecked_into();

        let playground_keydown =
            make_closure(move |kbevent| game.borrow_mut().on_keydown_in_playground(kbevent));
        let vkb_keyup = make_closure(move |kbevent| game.borrow().on_keyup_in_playground(kbevent));
        let main_menu_keydown =
            make_closure(move |kbevent| game.borrow().on_keydown_in_main_menu(kbevent));
        let main_menu_keyup =
            make_closure(move |kbevent| game.borrow_mut().on_keyup_in_main_menu(kbevent));

        let escape = GenericVirtualKey::new(
            "Escape",
            document
                .get_element_by_id("escape")
                .expect("there is no escape"),
        );

        let mut new = SceneManager {
            state: game,
            window,
            document,
            scene: Scene::Playground,
            playground,
            main_menu,
            escape,
            playground_keydown,
            vkb_keyup,
            main_menu_keydown,
            main_menu_keyup,
        };
        new.setup_scene();
        new
    }

    fn setup_scene(&mut self) {
        use Scene::*;

        match &self.scene {
            Playground => {
                self.playground.set_attribute("offscreen", "no").unwrap();
                self.main_menu.set_attribute("offscreen", "left").unwrap();

                self.window
                    .add_event_listener_with_callback(
                        "keydown",
                        self.playground_keydown.as_ref().unchecked_ref(),
                    )
                    .unwrap();
                self.window
                    .remove_event_listener_with_callback(
                        "keydown",
                        self.main_menu_keydown.as_ref().unchecked_ref(),
                    )
                    .unwrap();
                self.window
                    .remove_event_listener_with_callback(
                        "keyup",
                        self.main_menu_keyup.as_ref().unchecked_ref(),
                    )
                    .unwrap();

                if self.state.borrow().show_virtual_keyboard {
                    self.window
                        .add_event_listener_with_callback(
                            "keyup",
                            self.vkb_keyup.as_ref().unchecked_ref(),
                        )
                        .unwrap();
                }

                if let Some(element) = self.document.active_element() {
                    element.unchecked_into::<HtmlElement>().blur().unwrap();
                }
            }
            MainMenu => {
                self.playground.set_attribute("offscreen", "right").unwrap();
                self.main_menu.set_attribute("offscreen", "no").unwrap();

                self.window
                    .remove_event_listener_with_callback(
                        "keydown",
                        self.playground_keydown.as_ref().unchecked_ref(),
                    )
                    .unwrap();

                self.window
                    .add_event_listener_with_callback(
                        "keydown",
                        self.main_menu_keydown.as_ref().unchecked_ref(),
                    )
                    .unwrap();
                self.window
                    .add_event_listener_with_callback(
                        "keyup",
                        self.main_menu_keyup.as_ref().unchecked_ref(),
                    )
                    .unwrap();

                if self.state.borrow().show_virtual_keyboard {
                    self.window
                        .remove_event_listener_with_callback(
                            "keyup",
                            self.vkb_keyup.as_ref().unchecked_ref(),
                        )
                        .unwrap();
                }
            }
        }
    }

    fn on_escape(&mut self) {
        use Scene::*;

        self.scene = match self.scene {
            Playground => MainMenu,
            _ => Playground,
        };

        self.setup_scene()
    }
}

#[wasm_bindgen(start)]
pub fn run() {
    // Initialize debugging for when/if something goes wrong.
    console_error_panic_hook::set_once();

    let window = web_sys::window().unwrap();

    let mut scene_manager = SceneManager::new(window.clone());
    let main_callback = make_closure(move |kbevent: KeyboardEvent| {
        if kbevent.code() == "Escape" && !kbevent.repeat() {
            match kbevent.type_().as_str() {
                "keydown" => scene_manager.escape.hold(),
                "keyup" => {
                    scene_manager.escape.release();
                    scene_manager.on_escape()
                }
                _ => unreachable!(),
            }
        }
    });
    window
        .add_event_listener_with_callback("keyup", main_callback.as_ref().unchecked_ref())
        .unwrap();
    window
        .add_event_listener_with_callback("keydown", main_callback.as_ref().unchecked_ref())
        .unwrap();
    main_callback.forget();
}
