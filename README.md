# Z E N   T Y P I N G

A casual typing game that is language and keyboard layout agnostic.

[▶️ P L A Y](https://thibaultlemaire.gitlab.io/zen-typing)

This README is focused on setting up a development environment and technical details. If you want information about the game itself, please see [the wiki] or read [my blog post].

[the wiki]: https://gitlab.com/ThibaultLemaire/zen-typing/-/wikis/home

## Cool related FOSS projects

- [Typewrite Someting](https://github.com/bozdoz/typewritesomething)
- [terminal-typeracer](https://gitlab.com/ttyperacer/terminal-typeracer)
- [KTouch](https://invent.kde.org/education/ktouch)

## I have a question but I can't (easily) find the answer anywhere

If you have a question about the project (technical or not), feel free to [open an issue], or just [email me].

If you can't find a piece of information, it's not that I don't want to divulge it, on the contrary, it's just that I've been too lazy to write it down 🙂

Besides, different people will have different interests, I may write a whole page of documentation on [why I chose Rust], while all you wanted to know was [why I used arbitrary symbols and not latin alphabet letters][my blog post].

So ask away! I don't bite, and it'll force me to grow the documentation.

[open an issue]: https://gitlab.com/ThibaultLemaire/zen-typing/-/issues
[email me]: thibault.lemaire@protonmail.com
[why I chose Rust]: https://fsharpforfunandprofit.com/posts/designing-with-types-making-illegal-states-unrepresentable/

## Useful commands

This project uses [just] to collect useful commands. To get a list of available commands:

```
just --list
```

[just]: https://github.com/casey/just

[my blog post]: https://thibaultlemaire.gitlab.io/games/zen-typing
