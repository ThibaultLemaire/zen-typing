FROM zen-typing-nix

RUN mkdir /tmp \
&& rustup default stable \
&& rustup target add wasm32-unknown-unknown